using UnityEngine;
using DG.Tweening;

public class PowerupAnimation : MonoBehaviour
{
    private void Start()
    {
        transform.DOMoveY(transform.position.y + 1.2f, 2f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }
    void Update()
    {
        transform.Rotate(0, 50 * Time.deltaTime, 0);
    }
}
