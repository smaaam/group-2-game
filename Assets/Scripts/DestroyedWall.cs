using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyedWall : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 2f);
    }

    public void DestroyObject()
    {
        Destroy(gameObject);
    }
}
