using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FinishScreen : MonoBehaviour
{
    [SerializeField] PlayerController player;
    [SerializeField] Timer timer;

    [SerializeField] GameObject goldMedal, silverMedal, bronzeMedal;

    [SerializeField] TextMeshProUGUI timeText, medalText;
    float playerTime;
    string medal = "Bronze";
    private void OnEnable()
    {
        timer.FinalStopTimer();
        playerTime = timer.GetFinalTime();
        timeText.text = playerTime.ToString("0.00");
        CheckMedal();
        medalText.text = medal + " MEDAL!";
    }
    private void CheckMedal()
    {
        if (playerTime <= 50f)
        {
            medal = "GOLD";
            goldMedal.SetActive(true);
        }
        else if(playerTime <= 70f)
        {
            medal = "SILVER";
            silverMedal.SetActive(true);
        }
        else
        {
            medal = "BRONZE";
            bronzeMedal.SetActive(true);
        }
    }
    #region ONCLICKS
    public void OnClick_Restart()
    {
        timer.ResetTimer();
        player.MoveToStartingPosition();
        player.StopMovement();
        GameStartManager.instance.Start321Sequence();

        this.gameObject.SetActive(false);
        AudioMusicManager.instance.RestartSong();

    }
    public void OnClick_MainMenu()
    {
        //TODO: vaihda scene mainmenu scenee
    }
    public void OnClick_Quit()
    {
        Application.Quit();
    }


    #endregion
}
