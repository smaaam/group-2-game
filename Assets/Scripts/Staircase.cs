using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staircase : MonoBehaviour
{
    public bool isLadder = false; //Jos on ladder kysees ni pist� trueksi inspectoris
    // Add to location where the ladder and/or staircase begins.

    PlayerController playerController;                      // Needed for deactivating player movement briefly.
    [SerializeField] GameObject player;

    public GameObject staircaseLocation;                // GameObject holding the location information.
    Vector3 moveToLocation;                             // GameObject location we are moving to.
    private bool enterSuccess;                          

    // Start is called before the first frame update
    void Start()
    {
        playerController= player.GetComponent<PlayerController>();
        moveToLocation = staircaseLocation.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("UsingStaircase");
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.CompareTag("Player"))        // For safety checking if the collided object is player
        {
            enterSuccess = true;
            ClimbTextManager.instance.OpenText();
        }
    }

    void OnTriggerExit()
    {
        enterSuccess = false;
        ClimbTextManager.instance.CloseText();
    }

    IEnumerator UsingStaircase()
    {
        if(Input.GetKeyDown(KeyCode.E) && enterSuccess == true )
        {
            playerController.StopMovement();              // Disable player movement for the anim duration.
            AudioManager.instance.PlayLadderSound();

            if (isLadder)
            {
                playerController.PlayLadderAnimation();
            }
            else
            {
                playerController.PlayStairsAnimation();
            }

            yield return new WaitForSeconds(1f);
            player.transform.position = moveToLocation;         // Move to a new location
            playerController.canMove = true;               // Giving control back to the player     

            if(isLadder)
                playerController.ClosePlayLadderAnimation();
        }
    }
}
