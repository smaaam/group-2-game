using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    public GameObject timerBox;
    public TextMeshProUGUI completionTime;

    public float currentTime;
    public float finalTime;
    public float stopTest = 60.0f;          // Placeholder
    public bool timerActive = false;        // Bool for setting timer on and off

    void Start()
    {
        timerBox.gameObject.SetActive(true);                            // Activating timerBox to show
        //Startataan nyky��n GameStartManagerista
    }

    void Update()
    {
        if(timerActive == true)                 
        {
            currentTime = currentTime + Time.deltaTime;
        }
        TimeSpan time = TimeSpan.FromSeconds(currentTime);              // Updates seconds to minutes and so on..
        completionTime.text = "Time: " + time.ToString(@"mm\.ss\.ff");      // Updating the timer's text

        if(currentTime >= stopTest)                 // Mostly for testing purposes and when the game ends.
        {
            FinalStopTimer();                
        }
    }

    public void StartTimer()                        
    {
        timerActive = true;                         // Starts the timer
       
    }

    public void StopTimer() 
    {
        timerActive = false; 
    }

    public void FinalStopTimer()
    {
        timerActive = false;                        // Stops the timer
        timerBox.gameObject.SetActive(false);       // Stops showing the timer in game screen. Should only be used when the player completes the escaperoom.
        finalTime = currentTime;                    // Records the finalTime when the player completes the escaperoom.
    }
    public void ResetTimer()
    {
        timerBox.gameObject.SetActive(true);
        StopTimer();
        currentTime = 0;
    }
    public float GetFinalTime()
    {
        return finalTime;
    }
}
