
public interface IPowerup
{
    public void AddPowerupBoost();
    public void RemovePowerupBoost();

}
