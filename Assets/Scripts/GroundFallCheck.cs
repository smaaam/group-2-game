using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundFallCheck : MonoBehaviour
{
    [SerializeField] Transform startingPosition;
    void Update()
    {
        if(gameObject.transform.position.y <= -4)
        {
            this.gameObject.transform.position = startingPosition.transform.position;
        }
    }
}