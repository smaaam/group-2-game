using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator anim;
    private RotatePlayerModel rotate;
    private bool jumpCooldown = false;
    private Rigidbody playerRb;
    private bool jumpKeyWasPressed;
    private float horizontalInput;

    public bool speedPowerupActive = false;
    public bool jumpPowerUp = false;
    public int wallDestroyerUses = 0;
    public float speed = 10.0f;
    public float jumpForce;
    public float gravityModifier;
    public Vector3 gravity;
    public bool umbrella = false;
    public bool canMove = true;

    [SerializeField] private Transform groundCheckTransform;
    [SerializeField] private LayerMask playerMask;

    //Powerupit
    [SerializeField] float speedBoostTime, jumpBoostTime, umbrellaBoostTime;
    //Finishscreen
    [SerializeField] GameObject finishScreen;
    Vector3 startingPosition;

    private void Awake()
    {
        anim = this.gameObject.GetComponentInChildren<Animator>();
        rotate = this.gameObject.GetComponentInChildren<RotatePlayerModel>();
    }
    void Start()
    {
        gravity = Physics.gravity;
        playerRb = GetComponent<Rigidbody>();
        Physics.gravity *= gravityModifier;
        startingPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            horizontalInput = Input.GetAxis("Horizontal");

            anim.SetFloat("MoveSpeed", horizontalInput);
            if(horizontalInput < 0)
            {
                rotate.MovingRight();
            }
            else if (horizontalInput > 0)
            {
                rotate.MovingLeft();
            }
            else
            {
                rotate.Idle();
            }

            if (Input.GetKeyDown(KeyCode.Space) && !jumpCooldown)
            {
                jumpKeyWasPressed = true;
            }

            if (umbrella && playerRb.velocity.y < 0)
            {
                playerRb.drag = 10;
            }
            if (playerRb.velocity.y >= 0)
            {
                playerRb.drag = 0;
            }
        }
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            playerRb.velocity = new Vector3(horizontalInput * speed, playerRb.velocity.y, 0);

            if (Physics.OverlapSphere(groundCheckTransform.position, 0.1f, playerMask).Length == 0)
            {
                return;
            }

            if (jumpKeyWasPressed)
            {
                jumpCooldown = true;
                StartCoroutine(JumpCooldown());
                if(horizontalInput < 0.05f && horizontalInput > -0.05f)
                {
                    anim.SetTrigger("StationaryJump");
                }
                else
                {
                    anim.SetTrigger("RunningJump");
                }

                playerRb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
                jumpKeyWasPressed = false;
                AudioManager.instance.PlayJumpsound();
            }
        }   
    }
    private IEnumerator JumpCooldown()
    {
        yield return new WaitForSeconds(0.65f);
        jumpCooldown = false;
    }
    public void PlayLadderAnimation()
    {
        //Rotate pelaaja sein�� p�in
        rotate.LadderRotate();
        anim.SetBool("LadderClimbing", true);
    }
    public void ClosePlayLadderAnimation()
    {
        anim.SetBool("LadderClimbing", false);
    }
    public void PlayStairsAnimation()
    {
        rotate.MovingLeft();
        anim.SetTrigger("Stairs");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 7)
        {
            ApplyJumpPowerup();
            AudioManager.instance.PlayPowerupsound1();
            other.gameObject.SetActive(false);
            StartCoroutine(RespawnPowerup(other.gameObject));
            BoostTextManager.instance.ActivateJumpText();
            RadialBarManager.instance.PlayTimer(jumpBoostTime, "blue");
            Invoke("DisableJumpPowerup", jumpBoostTime);
            if (jumpPowerUp == true)
            {
                CancelInvoke("DisableJumpPowerup");
                Invoke("DisableJumpPowerup", 5f);
            }

        }

        if (other.gameObject.layer == 8)
        {
            ApplySpeedPowerup();
            AudioManager.instance.PlayPowerupsound2();
            other.gameObject.SetActive(false);
            StartCoroutine(RespawnPowerup(other.gameObject));
            BoostTextManager.instance.ActivateSpeedText();
            RadialBarManager.instance.PlayTimer(speedBoostTime, "red");
            Invoke("DisableSpeedPowerup", 5f);
            if (speedPowerupActive == true)
            {
                CancelInvoke("DisableSpeedPowerup");
                Invoke("DisableSpeedPowerup", 5f);
            }

        }

        if (other.gameObject.layer == 9)
        {
            ApplyUmbrellaPowerup();
            AudioManager.instance.PlayPowerupsound3();
            other.gameObject.SetActive(false);
            StartCoroutine(RespawnPowerup(other.gameObject));
            BoostTextManager.instance.ActivateUmbrellaText();
            RadialBarManager.instance.PlayTimer(umbrellaBoostTime, "brown");
            Invoke("DisableUmbrellaPowerup", 5f);
            if(umbrella == true)
            {
                CancelInvoke("DisableUmbrellaPowerup");
                Invoke("DisableUmbrellaPowerup", 3.5f);
            }
        }

        if (other.gameObject.layer == 10)
        {
            //Start game over screen, disable moving
            StopMovement();
            finishScreen.SetActive(true);
            AudioManager.instance.PlayFinishlinesound();
            //stop timer
        }

        if (other.gameObject.layer == 11)
        {
            ApplyWallDestroyer();
            other.gameObject.SetActive(false);
            AudioManager.instance.PlayPowerupsound3();
            StartCoroutine(RespawnPowerup(other.gameObject));
            BoostTextManager.instance.ActivateWallDestroyerText();
        }
    }

    //POWERUPS
    #region POWERUPS
    public void ApplySpeedPowerup()
    {
        speed = 20.0f;
        speedPowerupActive = true;
    }
    public void ApplyJumpPowerup()
    {
        jumpPowerUp = true;
        jumpForce = 24.0f;    
    }
    public void ApplyUmbrellaPowerup()
    {
        umbrella = true;
        anim.SetBool("Umbrella", true);
    }

    public void DisableSpeedPowerup()
    {
        speed = 10.0f;
        speedPowerupActive = false;
    }
    public void DisableJumpPowerup()
    {
        jumpForce = 15.0f;
        jumpPowerUp = false;
    }
    public void DisableUmbrellaPowerup()
    {
        playerRb.drag = 0;
        umbrella = false;
        anim.ResetTrigger("StationaryJump");
        anim.ResetTrigger("RunningJump");
        anim.SetBool("Umbrella", false);
    }

    public void ApplyWallDestroyer()
    {
        wallDestroyerUses = 1;
    }

    #endregion

    public void MoveToStartingPosition()
    {
        transform.position = startingPosition;
    }
    public void StopMovement()
    {
        horizontalInput = 0;
        anim.SetFloat("MoveSpeed", horizontalInput);
        canMove = false;

        rotate.IdleInstant();

        playerRb.velocity = Vector3.zero;
    }
    
    private IEnumerator RespawnPowerup(GameObject objekti)
    {
        yield return new WaitForSeconds(5f);
        objekti.SetActive(true);
    }
}
