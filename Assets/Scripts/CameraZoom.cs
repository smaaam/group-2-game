using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour
{
    float minFov = 40f;
    float maxFov = 70f;
    float currentFov = 50f;

    Camera camera;
    private void Awake()
    {
        camera = this.gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetAxis("Mouse ScrollWheel") > 0f) //Yl�s
        {
            currentFov -= 5f;
            if (currentFov <= minFov)
            {
                currentFov = minFov;
                camera.fieldOfView = currentFov;
                return;
            }
            camera.fieldOfView = currentFov;
        }
        else if(Input.GetAxis("Mouse ScrollWheel") < 0f) //Alas
        {

            currentFov += 5f;
            if (currentFov >= maxFov)
            {
                currentFov = maxFov;
                camera.fieldOfView = currentFov;
                return;
            }
            camera.fieldOfView = currentFov;
        }
    }
}
