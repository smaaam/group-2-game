using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayerModel : MonoBehaviour
{

    public void MovingLeft()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
    }
    public void MovingRight()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
    }
    public void Idle()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 180, 0), 300f * Time.deltaTime);
    }
    public void MoveTowardsWall()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, -180, 0));
    }
    public void LadderRotate()//Used in ladder
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 360, 0));
    }
    public void IdleInstant()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
    }
}
