using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableWall : MonoBehaviour
{
    public PlayerController playerController;
    [SerializeField] private Transform destroyedWall;
    private Vector3 playerPos;
    private void Start()
    {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        playerPos = GameObject.Find("Player").GetComponent<Transform>().position;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && playerController.wallDestroyerUses > 0)
        {
            Transform wallBrokenTransform = Instantiate(destroyedWall, transform.position, transform.rotation);
            foreach (Transform child in wallBrokenTransform)
            {
                if (child.TryGetComponent<Rigidbody>(out Rigidbody childRigidbody))
                {
                    childRigidbody.AddExplosionForce(1000f, playerPos, 5f);
                }
            }
            gameObject.SetActive(false);
            
            playerController.wallDestroyerUses = 0;
        }
    }
}
