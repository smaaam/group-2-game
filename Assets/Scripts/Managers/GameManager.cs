using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance{get; private set;}
    public GameObject inGameMenu;
    public GameObject destroyableWall1;
    public GameObject destroyableWall2;

    Timer timer;
    [SerializeField] GameObject inGameTimer;

    //Player
    [SerializeField] PlayerController player;
    void Awake()
    {
        Instance = this;
        timer = inGameTimer.GetComponent<Timer>();
        
    }
    
    void Update()
    {
        showGameMenu();
    }
    public void ReturnGame()
    {
        timer.StartTimer();
        inGameMenu.SetActive(false);
    }

    // Showing ingame menu when using esc
    public void showGameMenu()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            inGameMenu.gameObject.SetActive(true);
            timer.StopTimer();
        }
    }

    // Loading the Demo Scene
    public void StartDemo()
    {
        SceneManager.LoadScene("TestScene");
        Physics.gravity = new Vector3(0, -9.81f, 0);
    }

    // Reloading the current scene
    public void RestartStage()
    {
        player.MoveToStartingPosition();
        player.StopMovement();
        GameStartManager.instance.Start321Sequence();
        timer.ResetTimer();
        inGameMenu.SetActive(false);
        AudioManager.instance.PlayClickSound();
        destroyableWall1.gameObject.SetActive(true);
        destroyableWall2.gameObject.SetActive(true);
    }

    // Loading Main Menu
    public void ReturnToMenu()
    {
        AudioManager.instance.PlayClickSound();
        SceneManager.LoadScene("MainMenu");
    }

    // Shutting down the game
    public void QuitGame()
    {
        AudioManager.instance.PlayClickSound();
        Debug.Log("You've quit the game");
        Application.Quit();
    }
}
