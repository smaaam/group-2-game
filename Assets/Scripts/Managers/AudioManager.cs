using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    //Clipit
    [SerializeField] AudioClip powerupSound1, powerupSound2, powerupSound3, jumpSound, pauseSound, finishLineSound, startingSound, startingSound2, ladderSound, uiClickSound;

    AudioSource audioSource;

    public static AudioManager instance;

    private void Awake() //Singleton
    {
        instance = this;

        audioSource = this.gameObject.GetComponent<AudioSource>();
    }

    public void PlayPowerupsound1()
    {
        audioSource.PlayOneShot(powerupSound1);
    }

    public void PlayPowerupsound2()
    {
        audioSource.PlayOneShot(powerupSound2);
    }

    public void PlayPowerupsound3()
    {
        audioSource.PlayOneShot(powerupSound3);
    }

    public void PlayJumpsound()
    {
        audioSource.PlayOneShot(jumpSound);
    }
    public void PlayPauseupsound()
    {
        audioSource.PlayOneShot(pauseSound);
    }
    public void PlayFinishlinesound()
    {
        audioSource.PlayOneShot(finishLineSound);
    }
    public void PlayStartingSound()
    {
        audioSource.PlayOneShot(startingSound);
    }
    public void PlayStartingSound2()
    {
        audioSource.PlayOneShot(startingSound2);
    }
    public void PlayLadderSound()
    {
        audioSource.PlayOneShot(ladderSound);
    }
    public void PlayClickSound()
    {
        audioSource.PlayOneShot(uiClickSound);
    }
}
