using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMusicManager : MonoBehaviour
{
    AudioSource source;
    public static AudioMusicManager instance;
    private void Awake()
    {
        instance = this;
        source = this.gameObject.GetComponent<AudioSource>();
    }

    public void RestartSong()
    {
        source.Stop();
        source.Play();
    }
}
