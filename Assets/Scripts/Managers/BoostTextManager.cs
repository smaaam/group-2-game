using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostTextManager : MonoBehaviour
{
    [SerializeField] GameObject speedText, jumpText, umbrellaText, wallDestroyerText;
    Animator speedAnim, jumpAnim, umbrellaAnim, wallDestroyerAnim;

    public static BoostTextManager instance;
    private void Awake() //Singleton
    {
        instance = this;

        speedAnim = speedText.GetComponent<Animator>();
        jumpAnim = jumpText.GetComponent<Animator>();
        umbrellaAnim = umbrellaText.GetComponent<Animator>();
        wallDestroyerAnim = wallDestroyerText.GetComponent<Animator>();
        speedText.SetActive(false);
        jumpText.SetActive(false);
        umbrellaText.SetActive(false);
    } 

    public void ActivateSpeedText()
    {
        speedText.SetActive(true);
        speedAnim.SetTrigger("activate");
    }
    public void ActivateJumpText()
    {
        jumpText.SetActive(true);
        jumpAnim.SetTrigger("activate");
    }
    public void ActivateUmbrellaText()
    {
        umbrellaText.SetActive(true);
        umbrellaAnim.SetTrigger("activate");
    }

    public void ActivateWallDestroyerText()
    {
        wallDestroyerText.SetActive(true);
        wallDestroyerAnim.SetTrigger("activate");
    }
}
