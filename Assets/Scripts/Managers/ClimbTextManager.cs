using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbTextManager : MonoBehaviour
{
    [SerializeField] GameObject textObject;

    public static ClimbTextManager instance;
    private void Awake()
    {
        instance = this;

        textObject.SetActive(false);
    }

    public void OpenText()
    {
        textObject.SetActive(true);
    }
    public void CloseText()
    {
        textObject.SetActive(false);
    }
}
