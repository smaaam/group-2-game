using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialBarManager : MonoBehaviour
{
    //RadialWaitBar
    [SerializeField] private Image fill;
    [SerializeField] private GameObject radialWaitBar;
    private bool coroRunning = false;
    private Coroutine coro;

    //mats
    [SerializeField] Color blue, red, brown;
    public static RadialBarManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        radialWaitBar.SetActive(false);
    } // Singleton
    private void Start()
    {
        //Declare coroutine
        //(Pit�� alottaa ja heti lopettaa coroutine ett� saa variablen "k�ytt��n")
        coro = StartCoroutine(PlayRadialWaitBar(5));
        StopCoroutine(coro);
        radialWaitBar.SetActive(false);
    }

    public void PlayTimer(float duration, string color)
    {
        if (coroRunning)
        {
            StopCoroutine(coro);

            coro = StartCoroutine(PlayRadialWaitBar(duration));
        }
        else
        {
            coro = StartCoroutine(PlayRadialWaitBar(duration));
        }

        if(color == "blue")
            fill.color = blue;
        else if(color == "red")
            fill.color = red;
        else if(color == "brown")
            fill.color = brown;

    }
    private IEnumerator PlayRadialWaitBar(float duration)
    {
        coroRunning = true;
        radialWaitBar.SetActive(true);
        float normalizedTime = 0;
        while (normalizedTime <= duration)
        {
            fill.fillAmount = normalizedTime / duration;
            normalizedTime += Time.deltaTime;
            yield return null;
        }
        normalizedTime = duration;
        fill.fillAmount = normalizedTime;
        radialWaitBar.SetActive(false);
        fill.fillAmount = 0;
        coroRunning = false;
    }
}
