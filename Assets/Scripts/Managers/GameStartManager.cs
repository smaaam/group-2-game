using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameStartManager : MonoBehaviour
{
    // Display finish line with camera and then move camera back to player
    // After that display 3-2-1-GO
    //
    [SerializeField] GameObject mainCamera;
    CameraFollow cameraFollow;
    [SerializeField] PlayerController player;

    [SerializeField] GameObject finishingPoint, three, two, one, go, finishlineText;

    Vector3 endValue;
    Vector3 originalPosition;

    [SerializeField] Timer timer;
    [SerializeField] PowerupParent powerupParent;

    public bool skipCameraTransitions = false;

    public static GameStartManager instance;
    private void Awake() //Singleton
    {
        instance = this;

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        cameraFollow = mainCamera.GetComponent<CameraFollow>();
    }

    private void Start()
    {
        endValue = new Vector3(finishingPoint.transform.position.x, finishingPoint.transform.position.y, mainCamera.transform.position.z);
        originalPosition = mainCamera.transform.position;

        ShowFinishLine();
    }

    private void ShowFinishLine()
    {
        player.canMove = false;
        cameraFollow.canFollowPlayer = false;
        if (skipCameraTransitions)
        {
            Start321Sequence();
            cameraFollow.canFollowPlayer = true;
        }
        else
        {
            StartCoroutine(StartCameraSequence());
        }

    }
    private IEnumerator StartCameraSequence()
    {
        yield return new WaitForSeconds(3f);

        mainCamera.transform.DOMove(endValue, 6f, false);
        yield return new WaitForSeconds(5.5f);
        finishlineText.SetActive(true);
        yield return new WaitForSeconds(4.5f);
        mainCamera.transform.DOMove(originalPosition, 6f, false);
        yield return new WaitForSeconds(6.5f);
        Start321Sequence();
        cameraFollow.canFollowPlayer = true;
    }

    public void Start321Sequence()
    {
        StartCoroutine(GameStartCountdown());
    }
    private IEnumerator GameStartCountdown()
    {
        //Fix powerups
        powerupParent.EnableAllPowerups();
        yield return new WaitForSeconds(1f);
        three.SetActive(true);
        yield return new WaitForSeconds(1f);
        two.SetActive(true);
        yield return new WaitForSeconds(1f);
        one.SetActive(true);
        yield return new WaitForSeconds(1f);
        go.SetActive(true);
        yield return new WaitForSeconds(1f);
        player.canMove = true;
        timer.StartTimer();
        //start timer here

    }
}
