using UnityEngine;

public class DisableGameobject : MonoBehaviour
{
    public void DisableThisGameobject()
    {
        this.gameObject.SetActive(false);
    }
}
