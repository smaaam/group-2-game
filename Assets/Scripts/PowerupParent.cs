using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupParent : MonoBehaviour
{
    
    //This script sets all powerups to active when restarting the game 
    [SerializeField] GameObject[] powerups;

    public void EnableAllPowerups()
    {
        foreach(GameObject powerup in powerups)
        {
            powerup.SetActive(true);
        }
    }
}
