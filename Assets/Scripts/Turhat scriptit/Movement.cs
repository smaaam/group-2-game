using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private float moveSpeed = 20f;

    //Jump
    public float jumpTime = 0.3f;
    public float jumpHeight = 5;
    public bool isGrounded;
    public float gravity = 15f;

    //Ground detector rays
    [SerializeField] GameObject detector1, detector2, roofdetector1, roofdetector2;

    //Powerups
    public bool speedPowerupActive = false;
    public bool jumpPowerupActive = false;

    //Can move bool
    private bool canMove = true;

    public Coroutine coro;
    private void Start()
    {
        coro = StartCoroutine(JumpLerp());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            print("GROUNDED");
            isGrounded = true;
        }
    }

    private void Update()
    {
        if (canMove)
        {
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(new Vector3(-1, 0, 0) * Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime * moveSpeed);
            }

            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (speedPowerupActive)
                {
                    moveSpeed = 60f;
                }
                else
                {
                    moveSpeed = 40f;
                }
            }
            else
            {
                if (speedPowerupActive)
                {
                    moveSpeed = 40f;
                }
                else
                {
                    moveSpeed = 20f;
                }
            }

            if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
            {
                print("jump");
                coro = StartCoroutine(JumpLerp());
                isGrounded = false;
            }
        }

        //Raycast ground detect
        RaycastHit hit;
        if (Physics.Raycast(detector1.transform.position, transform.TransformDirection(Vector3.down), out hit, 50f))
        {
            Debug.DrawRay(detector1.transform.position, transform.TransformDirection(Vector3.down) * hit.distance, Color.green);
            if (hit.distance > 0.1f && hit.collider.CompareTag("Ground"))
            {
                isGrounded = false;
            }
            else
            {
                isGrounded = true;
            }
        }

        RaycastHit hit2;
        if (Physics.Raycast(detector2.transform.position, transform.TransformDirection(Vector3.down), out hit2, 50f))
        {
            Debug.DrawRay(detector2.transform.position, transform.TransformDirection(Vector3.down) * hit2.distance, Color.green);
            if (hit2.distance > 0.1f && hit2.collider.CompareTag("Ground"))
            {
                isGrounded = false;
            }
            else
            {
                isGrounded = true;
            }
        }
        //Roof detector raycast
        RaycastHit hit3;
        if (Physics.Raycast(roofdetector1.transform.position, transform.TransformDirection(Vector3.up), out hit3, 50f))
        {
            Debug.DrawRay(roofdetector1.transform.position, transform.TransformDirection(Vector3.up) * hit3.distance, Color.green);
            if (hit3.distance < 0.2f && hit3.collider.CompareTag("Ground"))
            {
                print("HIT ROOF");
                StopCoroutine(coro);
                isGrounded = false;
            }
        }
        RaycastHit hit4;
        if (Physics.Raycast(roofdetector2.transform.position, transform.TransformDirection(Vector3.up), out hit4, 50f))
        {
            Debug.DrawRay(roofdetector2.transform.position, transform.TransformDirection(Vector3.up) * hit4.distance, Color.green);
            if (hit4.distance < 0.2f && hit4.collider.CompareTag("Ground"))
            {
                print("HIT ROOF");
                StopCoroutine(coro);
                isGrounded = false;
            }
        }

        if (!isGrounded)
        {
            transform.position += new Vector3(0, -gravity * Time.deltaTime, 0);
        }
        if (transform.position.y <= 0.5)
        {
            transform.position = new Vector3(transform.position.x, 0.5f, transform.position.z);
            isGrounded = true;
        }
    }
    private IEnumerator JumpLerp()
    {
        yield return null;

        float actualTime = 0f;
        float timer = .3f;
        float rate = .5f; // Time.deltaTime; ja time.deltatime * rate?
        Vector3 endPos = new Vector3(transform.position.x, transform.position.y + jumpHeight, transform.position.z);
        while (actualTime < timer)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, endPos.y, transform.position.z), actualTime / timer);
            actualTime += Time.deltaTime * rate;
            yield return null;
        }
        transform.position = new Vector3(transform.position.x, endPos.y, transform.position.z);
    }

    //POWERUPS
    #region POWERUPS
    public void ApplySpeedPowerup()
    {
        speedPowerupActive = true;
    }
    public void ApplyJumpPowerup()
    {
        jumpHeight = 15f;
        jumpTime = 0.6f;
    }
    public void ApplyUmbrellaPowerup()
    {
        gravity = 5f;
    }

    public void DisableSpeedPowerup()
    {
        speedPowerupActive = false;
    }
    public void DisableJumpPowerup()
    {
        jumpHeight = 4f;
        jumpTime = 0.3f;
    }
    public void DisableUmbrellaPowerup()
    {
        gravity = 15f;
    }

    #endregion
}

