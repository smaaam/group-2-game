using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class JumpPowerUp : MonoBehaviour, IPowerup
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject pogostickMesh;
    Movement movement;
    [SerializeField] float boostTime = 5f;


    BoxCollider collider;
    private void Awake()
    {
        movement = player.GetComponent<Movement>();
        collider = this.gameObject.GetComponent<BoxCollider>();

        transform.DOMoveY(transform.position.y + 1.2f, 2f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }
    void Update()
    {
        transform.Rotate(0, 50 * Time.deltaTime, 0);
    }
    public void AddPowerupBoost()
    {
        movement.ApplyJumpPowerup();
        BoostTextManager.instance.ActivateJumpText();
        RadialBarManager.instance.PlayTimer(boostTime, "blue");
        Invoke("RemovePowerupBoost", boostTime);
    }

    public void RemovePowerupBoost()
    {
        movement.DisableJumpPowerup();
    }
    public void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.CompareTag("Player"))
        {
            AddPowerupBoost();
            collider.enabled = false;
            pogostickMesh.SetActive(false);
        }
    }
}
