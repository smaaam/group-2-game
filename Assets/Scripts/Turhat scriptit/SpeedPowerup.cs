using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SpeedPowerup : MonoBehaviour, IPowerup
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject shoeMesh;
    Movement movement;
    [SerializeField] float boostTime = 5f;

    SphereCollider collider;
    private void Awake()
    {
        movement = player.GetComponent<Movement>();
        collider = this.gameObject.GetComponent<SphereCollider>();

        transform.DOMoveY(transform.position.y + 1.2f, 2f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }
    void Update()
    {
        transform.Rotate(0, 50 * Time.deltaTime, 0);
    }
    public void AddPowerupBoost()
    {
        movement.ApplySpeedPowerup();
        BoostTextManager.instance.ActivateSpeedText();
        RadialBarManager.instance.PlayTimer(boostTime, "red");
        Invoke("RemovePowerupBoost", boostTime);
        
    }

    public void RemovePowerupBoost()
    {
        movement.DisableSpeedPowerup();
    }
    public void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.CompareTag("Player"))
        {
            AddPowerupBoost();
            collider.enabled = false;
            shoeMesh.SetActive(false);
        }
    }
}
