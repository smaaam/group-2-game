using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UmbrellaPowerup : MonoBehaviour, IPowerup
{
    [SerializeField] GameObject player;
    [SerializeField] GameObject umbrellaMesh;
    Movement movement;
    [SerializeField] float boostTime = 5f;

    SphereCollider collider;
    MeshRenderer renderer;
    private void Awake()
    {
        movement = player.GetComponent<Movement>();
        collider = this.gameObject.GetComponent<SphereCollider>();
        renderer = umbrellaMesh.gameObject.GetComponent<MeshRenderer>();

        transform.DOMoveY(transform.position.y + 1.2f, 2f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }
    void Update()
    {
        transform.Rotate(0, 50 * Time.deltaTime, 0);
    }
    public void AddPowerupBoost()
    {
        movement.ApplyUmbrellaPowerup();
        BoostTextManager.instance.ActivateUmbrellaText();
        RadialBarManager.instance.PlayTimer(boostTime, "brown");
        Invoke("RemovePowerupBoost", boostTime);

    }

    public void RemovePowerupBoost()
    {
        movement.DisableUmbrellaPowerup();
    }
    public void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.CompareTag("Player"))
        {
            AddPowerupBoost();
            collider.enabled = false;
            umbrellaMesh.SetActive(false);
        }
    }
}
